require 'httpclient'
class Potepan::SuggestsController < ApplicationController
  def index
    if params[:keyword].blank?
      logger.error("keyword is blank")
      return render status: 400, json: { message: "keyword is blank" }
    end

    response = Potepan::Api.suggest(params[:keyword], params[:max_num])

    if response.status == 200
      render json: JSON.parse(response.body)
    else
      logger.error("Suggest request error. Status: #{response.status}")
      render status: response.status, json: { message: "Internal Server Error" }
    end
  end
end
