class Potepan::Api::SuggestsController < ApplicationController
  include ActionController::HttpAuthentication::Token::ControllerMethods
  before_action :authenticate

  def index
    if params[:keyword].blank?
      return render status: 400, json: { message: "keyword is blank" }
    end

    keyword = params[:keyword]
    max_num = if params[:max_num].to_i.positive?
                [params[:max_num].to_i, Constants::API_SUGGESTS_LIMIT].min
              else
                Constants::API_SUGGESTS_LIMIT
              end
    render json: Potepan::Suggest.suggest_keyword(keyword).limit(max_num).pluck(:keyword)
  end

  private

  def authenticate
    authenticate_token || render_unauthorized
  end

  def authenticate_token
    authenticate_with_http_token do |token, options|
      ActiveSupport::SecurityUtils.secure_compare(token, ENV['POTEPANEC_SUGGESTS_API_KEY'])
    end
  end

  def render_unauthorized
    render status: 401, json: { message: "unauthorized" }
  end
end
