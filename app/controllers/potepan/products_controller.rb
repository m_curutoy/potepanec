class Potepan::ProductsController < ApplicationController
  MAX_DISPLAY_RELATED_PRODUCTS = 4

  def show
    @product = Spree::Product.find(params[:id])
    @relations = @product.relations.limit(MAX_DISPLAY_RELATED_PRODUCTS)
  end
end
