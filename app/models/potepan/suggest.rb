class Potepan::Suggest < ApplicationRecord
  scope :suggest_keyword, -> (keyword) { where("keyword LIKE ?", "#{keyword}%") }
end
