class Potepan::Api
  def self.suggest(keyword, max_num)
    url = ENV['POTEPAN_API_URL']
    query = { keyword: keyword, max_num: max_num }
    headers = { Authorization: "Bearer #{ENV['POTEPAN_API_KEY']}" }
    HTTPClient.get(url, query, headers)
  end
end
