FactoryBot.define do
  factory :keyword, class: 'Potepan::Suggest' do
    keyword { 'keyword' }
  end
end
