require 'rails_helper'

RSpec.describe Potepan::Suggest, type: :model do
  describe 'suggest_keyword' do
    subject { Potepan::Suggest.suggest_keyword(keyword) }

    let(:apache) { create(:keyword, keyword: "apache") }
    let(:bag1)   { create(:keyword, keyword: "bag1") }
    let(:bag2)   { create(:keyword, keyword: "bag2") }

    context 'keywordが"a"の場合' do
      let(:keyword) { 'a' }

      it '該当商品が１件表示されること' do
        is_expected.to match_array [apache]
      end
    end

    context 'keywordが"b"の場合' do
      let(:keyword) { 'b' }

      it '該当商品が２件が表示されること' do
        is_expected.to match_array [bag1, bag2]
      end
    end

    context 'keywordが"c"の場合' do
      let(:keyword) { 'c' }

      it '該当商品がないため、何も表示されないこと' do
        is_expected.to match_array []
      end
    end
  end
end
