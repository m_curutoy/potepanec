require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  let!(:taxon1) { create(:taxon) }
  let!(:taxon2) { create(:taxon) }
  let!(:taxon3) { create(:taxon) }
  let!(:product1) { create(:product, taxons: [taxon1, taxon2]) }
  let!(:product2) { create(:product, taxons: [taxon1, taxon2, taxon3]) }
  let!(:product3) { create(:product, taxons: [taxon2]) }
  let!(:product4) { create(:product, taxons: [taxon3]) }

  it "関連商品はtaxonが一致している" do
    expect(product1.relations).to contain_exactly(product2, product3)
  end

  it '関連商品にメインの商品は表示されない' do
    expect(product1.relations).not_to include product1
  end

  it 'taxonが重複していても表示は一度' do
    expect(product1.relations.count).to eq 2
  end
end
