require 'rails_helper'

RSpec.describe "PotepanApiSuggests", type: :request do
  let!(:keyword1) { create(:keyword, keyword: "ruby1") }
  let!(:keyword2) { create(:keyword, keyword: "ruby2") }
  let!(:keyword3) { create(:keyword, keyword: "ruby3") }
  let!(:keyword4) { create(:keyword, keyword: "ruby4") }
  let!(:keyword5) { create(:keyword, keyword: "ruby5") }
  let!(:keyword6) { create(:keyword, keyword: "ruby6") }
  let!(:params)   { { keyword: keyword, max_num: max_num } }

  before do
    get potepan_api_suggests_path,
        headers: { Authorization: "Bearer #{my_api_key}" },
        params: params
  end

  context 'トークン認証が正しい場合' do
    let(:my_api_key) { ENV['POTEPANEC_SUGGESTS_API_KEY'] }

    context 'keywordが有効な場合' do
      let(:keyword) { "r" }

      context 'max_numが５件以上の場合' do
        let(:max_num) { 5 }

        it 'status codeは200となること' do
          expect(response).to have_http_status(200)
        end

        it '5件以上は表示されない' do
          expect(JSON.parse(response.body)).to eq ["ruby1", "ruby2", "ruby3", "ruby4", "ruby5"]
          expect(JSON.parse(response.body)).not_to eq ["ruby6"]
        end
      end

      context 'max_numが0の場合' do
        let(:max_num) { 0 }

        it 'status codeは200となること' do
          expect(response).to have_http_status(200)
        end

        it '検索候補が全て表示されること' do
          expect(JSON.parse(response.body)).to eq ["ruby1", "ruby2", "ruby3", "ruby4", "ruby5", "ruby6"]
        end
      end

      context 'max_numが空白の場合' do
        let(:max_num) { nil }

        it 'status codeは200となること' do
          expect(response).to have_http_status(200)
        end

        it '検索候補が全て表示されること' do
          expect(JSON.parse(response.body)).to eq ["ruby1", "ruby2", "ruby3", "ruby4", "ruby5", "ruby6"]
        end
      end

      context 'max_numが数字以外の場合' do
        let(:max_num) { "max_num" }

        it 'status codeは200となること' do
          expect(response).to have_http_status(200)
        end

        it '検索候補が全て表示されること' do
          expect(JSON.parse(response.body)).to eq ["ruby1", "ruby2", "ruby3", "ruby4", "ruby5", "ruby6"]
        end
      end
    end

    context 'keywordが無効の場合' do
      let(:max_num) { 5 }

      context 'keywordがblankの場合' do
        let(:keyword) { nil }

        it 'status codeは400となること' do
          expect(response).to have_http_status(400)
        end

        it 'エラーメッセージが表示されること' do
          expect(JSON.parse(response.body)['message']).to include "keyword is blank"
        end
      end

      context 'keywordが空白のみの場合' do
        let(:keyword) { "  " }

        it 'status codeは400となること' do
          expect(response).to have_http_status(400)
        end

        it 'エラーメッセージが表示されること' do
          expect(JSON.parse(response.body)['message']).to include "keyword is blank"
        end
      end
    end
  end

  context 'トークン認証に誤りがある場合' do
    let(:keyword) { "r" }
    let(:max_num) { 5 }

    context 'api_keyがnilの場合' do
      let(:my_api_key) { nil }

      it 'status codeは401となること' do
        expect(response).to have_http_status(401)
      end

      it 'エラーメッセージが表示されること' do
        expect(JSON.parse(response.body)['message']).to include "unauthorized"
      end
    end

    context 'api_keyが誤っている場合' do
      let(:my_api_key) { "api_key" }

      it 'status codeは401となること' do
        expect(response).to have_http_status(401)
      end

      it 'エラーメッセージが表示されること' do
        expect(JSON.parse(response.body)['message']).to include "unauthorized"
      end
    end
  end
end
