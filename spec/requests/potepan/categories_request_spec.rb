require 'rails_helper'

RSpec.describe "Categories", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon)    { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  describe "GET #show" do
    before do
      get potepan_category_path(taxon.id)
    end

    it 'リクエストが成功すること' do
      expect(response).to have_http_status(200)
    end

    it 'showテンプレートが表示されること' do
      expect(response).to render_template :show
    end

    it '変数taxonomiesの中身が正しいこと' do
      expect(assigns(:taxonomies)).to contain_exactly(taxonomy)
    end

    it '変数taxonの中身が正しいこと' do
      expect(assigns(:taxon)).to eq taxon
    end

    it '変数productsの中身が正しいこと' do
      expect(assigns(:products)).to contain_exactly(product)
    end
  end
end
