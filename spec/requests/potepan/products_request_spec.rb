require 'rails_helper'

RSpec.describe "PotepanProducts", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon)    { create(:taxon, taxonomy: taxonomy) }
  let(:product)  { create(:product, taxons: [taxon]) }

  describe "GET #show" do
    before do
      get potepan_product_path(product.id)
    end

    it 'リクエストが成功すること' do
      expect(response).to have_http_status(200)
    end

    it 'showテンプレートが表示されること' do
      expect(response).to render_template :show
    end
  end
end
