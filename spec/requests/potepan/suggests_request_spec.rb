require 'rails_helper'
require 'webmock'

RSpec.describe "PotepanSuggests", type: :request do
  before do
    WebMock.enable!
    WebMock.stub_request(:get, ENV['POTEPAN_API_URL']).
      with(
        query: { keyword: keyword, max_num: 5 },
        headers: { Authorization: "Bearer #{ENV['POTEPAN_API_KEY']}" }
      ).
      to_return(
        body: body,
        status: status
      )
    get potepan_suggests_path, params: { keyword: keyword, max_num: 5 }
  end

  context 'リクエストが正常である場合' do
    let(:keyword) { "r" }
    let(:body)    { ["ruby1", "ruby2", "ruby3", "ruby4", "ruby5"].to_json }
    let(:status)  { 200 }

    it 'status codeは200となること' do
      expect(response).to have_http_status(200)
    end

    it 'bodyの中身が正しいこと' do
      expect(JSON.parse(response.body)).to eq ["ruby1", "ruby2", "ruby3", "ruby4", "ruby5"]
    end
  end

  context 'keywordがnilの場合' do
    let(:keyword) { nil }

    it 'status codeは400となること' do
      expect(response).to have_http_status(400)
    end

    it 'エラーメッセージが表示されること' do
      expect(JSON.parse(response.body)['message']).to include "keyword is blank"
    end
  end

  context '異常が生じた場合' do
    let(:keyword) { "r" }
    let(:body)    { ["ruby1", "ruby2", "ruby3", "ruby4", "ruby5"].to_json }
    let(:status)  { 500 }

    it 'status codeは500となること' do
      expect(response).to have_http_status(500)
    end

    it 'エラーメッセージが表示されること' do
      expect(JSON.parse(response.body)['message']).to include "Internal Server Error"
    end
  end
end
