require 'rails_helper'

RSpec.describe 'PotepanProducts', type: :system do
  let(:taxonomy)    { create(:taxonomy) }
  let(:taxon)       { create(:taxon, taxonomy: taxonomy) }
  let(:taxon2)      { create(:taxon, taxonomy: taxonomy) }
  let(:product)     { create(:product, name: "TestProduct", description: "test", price: 100, taxons: [taxon]) }
  let!(:relations)  { create_list(:product, 5, taxons: [taxon]) }
  let!(:relations2) { create(:product, taxons: [taxon2]) }

  before do
    visit potepan_product_path(product.id)
  end

  it 'title名が正しく表示されていること' do
    expect(page).to have_title 'TestProduct - BIGBAG Store'
  end

  it '変数productに正しい値が渡されていること' do
    expect(page).to have_content 'TestProduct'
    expect(page).to have_content 'test'
    expect(page).to have_content 100.to_s
  end

  it '一覧ページへ戻るをクリックすると、該当商品が属する商品一覧へ戻ること' do
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(product.taxons.first.id)
  end

  it '関連商品のみ表示されていること' do
    expect(page).to have_content relations.first.name
    expect(page).to have_content relations.first.display_price
    expect(page).not_to have_content relations2.name
  end

  it '関連商品は４件のみ表示されていること' do
    expect(page).to have_selector ".productBox", count: 4
    expect(page).not_to have_content relations[4]
  end

  it '関連商品をクリックすると、該当商品の詳細ページへ移動すること' do
    click_on relations.first.name
    expect(current_path).to eq potepan_product_path(relations.first.id)
  end
end
