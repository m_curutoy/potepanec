require 'rails_helper'

RSpec.describe 'PotepanCategory', type: :system do
  let(:taxonomy)  { create(:taxonomy) }
  let(:taxon)     { create(:taxon, name: "TestTaxon", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let(:taxon2)    { create(:taxon, name: "TestTaxon2", taxonomy: taxonomy) }
  let!(:product)  { create(:product, taxons: [taxon]) }
  let!(:product2) { create(:product, taxons: [taxon]) }
  let!(:product3) { create(:product, taxons: [taxon2]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  it 'title名が正しいこと' do
    expect(page).to have_title 'TestTaxon - BIGBAG Store'
  end

  it '商品カテゴリー欄にはtaxonomy(名前)/taxon(名前・件数)が表示されること' do
    within '.side-nav' do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content taxon.name
      expect(page).to have_content taxon.all_products.count
    end
  end

  it '商品カテゴリー欄のtaxonをクリックすると、該当taxonの商品情報のみ表示されること' do
    click_link 'TestTaxon'
    within '.category-product' do
      expect(page).to have_content product.name
      expect(page).to have_content product.price
      expect(page).to have_content product2.name
      expect(page).to have_content product2.price
      expect(page).not_to have_content product3.name
    end
  end

  it '画像をクリックすると詳細ページへ移動すること' do
    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
