require 'rails_helper'

RSpec.describe ApplicationHelper do
  include ApplicationHelper
  describe "full_title" do
    context 'page_title is not empty' do
      it 'page_title-base_titleが表示される' do
        expect(full_title('Testtitle')).to eq('Testtitle - BIGBAG Store')
      end
    end

    context 'page_title is empty' do
      it 'base_titleのみが表示される' do
        expect(full_title('')).to eq('BIGBAG Store')
      end
    end

    context 'page_title is nill' do
      it 'base_titleのみが表示される' do
        expect(full_title(nil)).to eq('BIGBAG Store')
      end
    end

    context 'no argument' do
      it 'base_titleが表示される' do
        expect(full_title).to eq('BIGBAG Store')
      end
    end
  end
end
